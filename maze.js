const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
];

let row = 0
let col = 0


//1. Make each cell of the maze a DIV
const maze = document.querySelector("#maze")
const createDivs = function(map){
for(let i = 0;i<map.length;i++){
const rowElement = map[i]
let block = " "

    for(let j = 0; j < rowElement.length;j++){
        const rowNum = rowElement[j]
        if(rowNum === "W"){
            block += '<div id ="cell' + j + "-" + i + '" class="block wall"></div>'
           }else if(rowNum === "S"){
            block += '<div class="block start"></div>'
           }else if(rowNum === "F"){
            block += '<div id ="cell' + j + "-" + i + '" class="block finish"></div>'
           }else{
            block += '<div id ="cell' + j + "-" + i + '" class="block space"></div>'
            // row = i
            // col = j
           }
           
           
    }
    maze.innerHTML += `<div class="row">${block}</div>`

}
}

createDivs(map)

function gameOver(){
    maze.innerHTML = "<div class='win'> You Win!</div>"
}

let playerPostionUD = 260;
let playerPostionLR = 8;

document.addEventListener('keydown', movePlayer);

row = 9;
col = 0;

function movePlayer(p){
let currentPos = map[row].charAt(col);
let nextPos;

if(p.code === "ArrowDown"){
    nextPos = map[row + 1].charAt(col)
    if(nextPos === " "){
        playerPostionUD += 20
        currentPos = nextPos
        row ++
        // console.log(currentPos)
    }else{
        // console.log(nextPos)
     }

}
if(p.code === "ArrowUp"){
    nextPos = map[row - 1].charAt(col)
    if(nextPos === " "){
        playerPostionUD -= 20
        currentPos = nextPos
        row--
        // console.log(nextPos)
    }else{
    // console.log(nextPos)
    }
}
if(p.code === "ArrowLeft"){
    nextPos = map[row].charAt(col - 1)
    if(nextPos === " "){
        playerPostionLR -= 20
        currentPos = nextPos
        col--
    }else{
        // console.log(nextPos)
}
}
    
if(p.code === "ArrowRight"){
    nextPos = map[row].charAt(col + 1)
    if(nextPos === " "){
        playerPostionLR += 20
        col++
        currentPos = nextPos
        // console.log(nextPos)
    }else if(nextPos === "F"){
        gameOver()
    }
}

document.getElementById("player").style.top = playerPostionUD + "px";
document.getElementById("player").style.left = playerPostionLR + "px";


}